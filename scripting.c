/* Copyright (C) 2002-2003 by George Williams */
/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.

 * The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*			   Yet another interpreter			      */

#include "mensisui.h"
#include <fontforge/gfile.h>
#include <fontforge/gresource.h>
#include <fontforge/utype.h>
#include <fontforge/ustring.h>
#include <fontforge/gkeysym.h>
#include <fontforge/chardata.h>
#include <unistd.h>
#include <math.h>
#include <setjmp.h>

static int verbose = -1;

struct dictentry {
    char *name;
    Val val;
};

struct dictionary {
    struct dictentry *entries;
    int cnt, max;
};

static struct dictionary globals;

typedef struct array {
    int argc;
    Val *vals;
} Array;

#define TOK_MAX	100
enum token_type { tt_name, tt_string, tt_tag, tt_number, tt_unicode,
	tt_lparen, tt_rparen, tt_comma, tt_eos,		/* eos is end of statement, semicolon, newline */
	tt_lbracket, tt_rbracket,
	tt_dot,
	tt_minus, tt_plus, tt_not, tt_bitnot, tt_colon,
	tt_mul, tt_div, tt_mod, tt_and, tt_or, tt_bitand, tt_bitor, tt_xor,
	tt_eq, tt_ne, tt_gt, tt_lt, tt_ge, tt_le,
	tt_assign, tt_pluseq, tt_minuseq, tt_muleq, tt_diveq, tt_modeq,
	tt_incr, tt_decr,

	tt_if, tt_else, tt_elseif, tt_endif, tt_while, tt_foreach, tt_endloop,
	tt_shift, tt_return,

	tt_eof,

	tt_error = -1
};

static struct tabnames {
    char *name;
    int offset;
    enum val_type type;
} maxp_names[] = {
    { "version", 0, v_ttf32pt },
    { "numGlyphs", 4, v_ttf16pt },
    { "maxPoints", 6, v_ttf16pt },
    { "maxContours", 8, v_ttf16pt },
    { "maxCompositePoints", 10, v_ttf16pt },
    { "maxCompositeContours", 12, v_ttf16pt },
    { "maxZones", 14, v_ttf16pt },
    { "maxTwilightPoints", 16, v_ttf16pt },
    { "maxStorage", 18, v_ttf16pt },
    { "maxFunctionDefs", 20, v_ttf16pt },
    { "maxInstructionDefs", 22, v_ttf16pt },
    { "maxStackElements", 24, v_ttf16pt },
    { "maxSizeOfInstructions", 26, v_ttf16pt },
    { "maxComponentElements", 28, v_ttf16pt },
    { "maxComponentDepth", 30, v_ttf16pt },
    { NULL }
};
static struct tabnames os2_names[] = {
    { "version", 0, v_ttf16pt },
    { "xAvgCharWidth", 2, v_ttf16pt },
    { "usWeightClass", 4, v_ttf16pt },
    { "usWidthClass", 6, v_ttf16pt },
    { "fsType", 8, v_ttf16pt },
    { "ySubscriptXSize", 10, v_ttf16pt },
    { "ySubscriptYSize", 12, v_ttf16pt },
    { "ySubscriptXOffset", 14, v_ttf16pt },
    { "ySubscriptYOffset", 16, v_ttf16pt },
    { "ySuperscriptXSize", 18, v_ttf16pt },
    { "ySuperscriptYSize", 20, v_ttf16pt },
    { "ySuperscriptXOffset", 22, v_ttf16pt },
    { "ySuperscriptYOffset", 24, v_ttf16pt },
    { "yStrikeoutSize", 26, v_ttf16pt },
    { "yStrikeoutPosition", 28, v_ttf16pt },
    { "sFamilyClass", 30, v_ttf16pt },
    { "panose1", 32, v_ttf8pt },
    { "panose2", 33, v_ttf8pt },
    { "panose3", 34, v_ttf8pt },
    { "panose4", 35, v_ttf8pt },
    { "panose5", 36, v_ttf8pt },
    { "panose6", 37, v_ttf8pt },
    { "panose7", 38, v_ttf8pt },
    { "panose8", 39, v_ttf8pt },
    { "panose9", 40, v_ttf8pt },
    { "panose10", 41, v_ttf8pt },
    { "ulUnicodeRange1", 42, v_ttf32pt },
    { "ulUnicodeRange2", 46, v_ttf32pt },
    { "ulUnicodeRange3", 50, v_ttf32pt },
    { "ulUnicodeRange4", 54, v_ttf32pt },
    { "achVendID", 58, v_ttf32pt },
    { "achVendID1", 58, v_ttf8pt },
    { "achVendID2", 59, v_ttf8pt },
    { "achVendID3", 60, v_ttf8pt },
    { "achVendID4", 61, v_ttf8pt },
    { "fsSelection", 62, v_ttf16pt },
    { "usFirstCharIndex", 64, v_ttf16pt },
    { "usLastCharIndex", 66, v_ttf16pt },
    { "sTypoAscender", 68, v_ttf16pt },
    { "sTypoDescender", 70, v_ttf16pt },
    { "sTypoLineGap", 72, v_ttf16pt },
    { "usWinAscent", 74, v_ttf16pt },
    { "usWinDescent", 76, v_ttf16pt },
    { "ulCodePageRange1", 78, v_ttf32pt },
    { "ulCodePageRange2", 82, v_ttf32pt },
    { "sxHeight", 86, v_ttf16pt },
    { "sCapHeight", 88, v_ttf16pt },
    { "usDefaultChar", 90, v_ttf16pt },
    { "usBreakChar", 92, v_ttf16pt },
    { "usMaxContext", 94, v_ttf16pt },
    { NULL }
};
static struct tabnames _hea_names[] = {
    { "version", 0, v_ttf32pt },
    { "ascender", 4, v_ttf16pt },
    { "vertTypoAscender", 4, v_ttf16pt },
    { "Ascender", 4, v_ttf16pt },
    { "descender", 6, v_ttf16pt },
    { "vertTypoDescender", 6, v_ttf16pt },
    { "Descender", 6, v_ttf16pt },
    { "lineGap", 8, v_ttf16pt },
    { "vertTypoLineGap", 8, v_ttf16pt },
    { "LineGap", 8, v_ttf16pt },
    { "advanceHeightMax", 10, v_ttf16pt },
    { "advanceWidthMax", 10, v_ttf16pt },
    { "minTopSideBearing", 12, v_ttf16pt },
    { "minLeftSideBearing", 12, v_ttf16pt },
    { "minBottomSideBearing", 14, v_ttf16pt },
    { "minRightSideBearing", 14, v_ttf16pt },
    { "yMaxExtent", 16, v_ttf16pt },
    { "xMaxExtent", 16, v_ttf16pt },
    { "caretSlopeRise", 18, v_ttf16pt },
    { "caretSlopeRun", 20, v_ttf16pt },
    { "caretOffset", 22, v_ttf16pt },
    { "metricDataFormat", 32, v_ttf16pt },
    { "numberOfHMetrics", 34, v_ttf16pt },
    { "numberOfLongVerMetrics", 34, v_ttf16pt },
    { NULL }
};
static struct tabnames head_names[] = {
    { "version", 0, v_ttf32pt },
    { "fontRevision", 4, v_ttf32pt },
    { "checkSumAdjustment", 8, v_ttf32pt },
    { "magicNumber", 12, v_ttf32pt },
    { "flags", 16, v_ttf16pt },
    { "unitsPerEm", 18, v_ttf16pt },
    { "created1", 20, v_ttf32pt },
    { "created2", 24, v_ttf32pt },
    { "modified1", 28, v_ttf32pt },
    { "modified2", 32, v_ttf32pt },
    { "xMin", 36, v_ttf16pt },
    { "yMin", 38, v_ttf16pt },
    { "xMax", 40, v_ttf16pt },
    { "yMax", 42, v_ttf16pt },
    { "macStyle", 44, v_ttf16pt },
    { "lowestRecPPEM", 46, v_ttf16pt },
    { "fontDirectionHit", 48, v_ttf16pt },
    { "indexToLocFormat", 50, v_ttf16pt },
    { "glyphDataFormal", 52, v_ttf16pt },
    { NULL }
};
static struct tabnames post_names[] = {
    { "version", 0, v_ttf32pt },
    { "italicAngle", 4, v_ttf32pt },
    { "underlinePosition", 8, v_ttf16pt },
    { "underlineThickness", 10, v_ttf16pt },
    { "isFixedPitch", 12, v_ttf32pt },
    { "minMemType42", 16, v_ttf32pt },
    { "maxMemType42", 20, v_ttf32pt },
    { "minMemType1", 24, v_ttf32pt },
    { "maxMemType1", 28, v_ttf32pt },
    { NULL }
};

static struct tabnamelist {
    uint32 tag;
    struct tabnames *tabnames;
} tabnamelist[] = {
    { CHR('O','S','/','2'), os2_names },
    { CHR('h','e','a','d'), head_names },
    { CHR('h','h','e','a'), _hea_names },
    { CHR('m','a','x','p'), maxp_names },
    { CHR('p','o','s','t'), post_names },
    { CHR('v','h','e','a'), _hea_names },
    { 0 , NULL }
};

typedef struct context {
    struct context *caller;
    Array a;		/* args */
    Array **dontfree;
    struct dictionary locals;
    FILE *script;
    unsigned int backedup: 1;
    unsigned int donteval: 1;
    unsigned int returned: 1;
    char tok_text[TOK_MAX+1];
    enum token_type tok;
    Val tok_val;
    Val return_val;
    Val trace;
    Val argsval;
    char *filename;
    int lineno;
    int ungotch;
    TtfView *curtfv;
    jmp_buf *err_env;
} Context;

struct keywords { enum token_type tok; char *name; } keywords[] = {
    { tt_if, "if" },
    { tt_else, "else" },
    { tt_elseif, "elseif" },
    { tt_endif, "endif" },
    { tt_while, "while" },
    { tt_foreach, "foreach" },
    { tt_endloop, "endloop" },
    { tt_shift, "shift" },
    { tt_return, "return" },
    { 0, NULL }
};

static const char *toknames[] = {
    "name", "string", "number", "unicode id", 
    "lparen", "rparen", "comma", "end of statement",
    "lbracket", "rbracket",
    "dot",
    "minus", "plus", "logical not", "bitwise not", "colon",
    "multiply", "divide", "mod", "logical and", "logical or", "bitwise and", "bitwise or", "bitwise xor",
    "equal to", "not equal to", "greater than", "less than", "greater than or equal to", "less than or equal to",
    "assignment", "plus equals", "minus equals", "mul equals", "div equals", "mod equals",
    "increment", "decrement",
    "if", "else", "elseif", "endif", "while", "foreach", "endloop",
    "shift", "return",
    "End Of File",
    NULL };

static void arrayfree(Array *a) {
    int i;

    for ( i=0; i<a->argc; ++i ) {
	if ( a->vals[i].type==v_str )
	    free(a->vals[i].u.sval);
	else if ( a->vals[i].type==v_arr )
	    arrayfree(a->vals[i].u.aval);
    }
    free(a->vals);
    free(a);
}

static Array *arraycopy(Array *a) {
    int i;
    Array *c;

    c = galloc(sizeof(Array));
    c->argc = a->argc;
    c->vals = galloc(c->argc*sizeof(Val));
    memcpy(c->vals,a->vals,c->argc*sizeof(Val));
    for ( i=0; i<a->argc; ++i ) {
	if ( a->vals[i].type==v_str )
	    c->vals[i].u.sval = copy(a->vals[i].u.sval);
	else if ( a->vals[i].type==v_arr )
	    c->vals[i].u.aval = arraycopy(a->vals[i].u.aval);
    }
return( c );
}

void DictionaryFree(struct dictionary *dica) {
    int i;

    if ( dica==NULL )
return;

    for ( i=0; i<dica->cnt; ++i ) {
	free(dica->entries[i].name );
	if ( dica->entries[i].val.type == v_str )
	    free( dica->entries[i].val.u.sval );
	if ( dica->entries[i].val.type == v_arr )
	    arrayfree( dica->entries[i].val.u.aval );
    }
    free( dica->entries );
}

static int DicaLookup(struct dictionary *dica,char *name,Val *val) {
    int i;

    if ( dica!=NULL && dica->entries!=NULL ) {
	for ( i=0; i<dica->cnt; ++i )
	    if ( strcmp(dica->entries[i].name,name)==0 ) {
		val->type = v_lval;
		val->u.lval = &dica->entries[i].val;
return( true );
	    }
    }
return( false );
}

static void DicaNewEntry(struct dictionary *dica,char *name,Val *val) {

    if ( dica->entries==NULL ) {
	dica->max = 10;
	dica->entries = galloc(dica->max*sizeof(struct dictentry));
    } else if ( dica->cnt>=dica->max ) {
	dica->max += 10;
	dica->entries = grealloc(dica->entries,dica->max*sizeof(struct dictentry));
    }
    dica->entries[dica->cnt].name = copy(name);
    dica->entries[dica->cnt].val.type = v_void;
    val->type = v_lval;
    val->u.lval = &dica->entries[dica->cnt].val;
    ++dica->cnt;
}


static void calldatafree(Context *c) {
    int i;

    for ( i=1; i<c->a.argc; ++i ) {	/* child may have freed some args itself by shifting, but argc will reflect the proper values none the less */
	if ( c->a.vals[i].type == v_str )
	    free( c->a.vals[i].u.sval );
	if ( c->a.vals[i].type == v_arrfree || (c->a.vals[i].type == v_arr && c->dontfree[i]!=c->a.vals[i].u.aval ))
	    arrayfree( c->a.vals[i].u.aval );
    }
    DictionaryFree(&c->locals);

    if ( c->script!=NULL )
	fclose(c->script);
}

static void traceback(Context *c) {
    int cnt = 0;
    while ( c!=NULL ) {
	if ( cnt==1 ) printf( "Called from...\n" );
	if ( cnt>0 ) fprintf( stderr, " %s: %d\n", c->filename, c->lineno );
	calldatafree(c);
	if ( c->err_env!=NULL )
	    longjmp(*c->err_env,1);
	c = c->caller;
	++cnt;
    }
    exit(1);
}

static void showtoken(Context *c,enum token_type got) {
    if ( got==tt_name || got==tt_string )
	fprintf( stderr, " \"%s\"\n", c->tok_text );
    else if ( got==tt_number )
	fprintf( stderr, " %d (0x%x)\n", c->tok_val.u.ival, c->tok_val.u.ival );
    else if ( got==tt_unicode )
	fprintf( stderr, " 0u%x\n", c->tok_val.u.ival );
    else
	fprintf( stderr, "\n" );
    traceback(c);
}

static void expect(Context *c,enum token_type expected, enum token_type got) {
    if ( got!=expected ) {
	fprintf( stderr, "%s: %d Expected %s, got %s",
		c->filename, c->lineno, toknames[expected], toknames[got] );
	if ( screen_display!=NULL ) {
	    static unichar_t umsg[] = { '%','h','s',':',' ','%','d',' ','E','x','p','e','c','t','e','d',' ','%','h','s',',',' ','g','o','t',' ','%','h','s',  0 };
	    GWidgetError(NULL,umsg,c->filename, c->lineno, toknames[expected], toknames[got] );
	}
	showtoken(c,got);
    }
}

static void unexpected(Context *c,enum token_type got) {
    fprintf( stderr, "%s: %d Unexpected %s found",
	    c->filename, c->lineno, toknames[got] );
    if ( screen_display!=NULL ) {
	static unichar_t umsg[] = { '%','h','s',':',' ','%','d',' ','U','n','e','x','p','e','c','t','e','d',' ','%','h','s',  0 };
	GWidgetError(NULL,umsg,c->filename, c->lineno, toknames[got] );
    }
    showtoken(c,got);
}

static void error( Context *c, char *msg ) {
    fprintf( stderr, "%s: %d %s\n", c->filename, c->lineno, msg );
    if ( screen_display!=NULL ) {
	static unichar_t umsg[] = { '%','h','s',':',' ','%','d',' ','%','h','s',  0 };
	GWidgetError(NULL,umsg,c->filename, c->lineno, msg );
    }
    traceback(c);
}

static void errors( Context *c, char *msg, char *name) {
    fprintf( stderr, "%s: %d %s: %s\n", c->filename, c->lineno, msg, name );
    if ( screen_display!=NULL ) {
	static unichar_t umsg[] = { '%','h','s',':',' ','%','d',' ','%','h','s',':',' ','%','h','s',  0 };
	GWidgetError(NULL,umsg,c->filename, c->lineno, msg, name );
    }
    traceback(c);
}

static int32 derefttfpt(Context *c,Val *val) {
    uint8 *ipt = val->u.ttfptval;
    /* data in truetype tables might be misaligned and will be bigendian */
    /* Bounds checking should already have been done */

    if ( val->type==v_ttf8pt )
return( (int8) ipt[0] );
    else if ( val->type==v_ttf16pt )
return( (int16) ((ipt[0]<<8)|ipt[1]) );
    else if ( val->type==v_ttf32pt )
return( (int32) ((ipt[0]<<24)|(ipt[1]<<16)|(ipt[2]<<8)|ipt[3]) );
    else
	error( c,"Internal error attempt to derefence something not dereferenceable" );
return( 0 );
}

static void setttfpt(Context *c,Val *val,int32 ival) {
    uint8 *ipt = val->u.ttfptval;
    /* data in truetype tables might be misaligned and will be bigendian */
    /* Bounds checking should already have been done */

    if ( val->type==v_ttf8pt )
	ipt[0] = ival;
    else if ( val->type==v_ttf16pt ) {
	ipt[0] = ival>>8;
	ipt[1] = ival;
    } else if ( val->type==v_ttf32pt ) {
	ipt[0] = ival>>24;
	ipt[1] = ival>>16;
	ipt[2] = ival>>8;
	ipt[3] = ival;
    } else
	error( c,"Internal error attempt to derefence something not dereferenceable" );
}

static void dereflvalif(Context *c,Val *val) {
    if ( val->type == v_lval ) {
	*val = *val->u.lval;
	if ( val->type==v_str )
	    val->u.sval = copy(val->u.sval);
    } else if ( val->type == v_ttf32pt || val->type == v_ttf16pt || val->type == v_ttf8pt ) {
	val->u.ival = derefttfpt(c,val);
	val->type = v_int;
    }
}

/* *************************** Built in Functions *************************** */

static void PrintVal(Val *val) {
    int j;

    if ( val->type==v_str )
	printf( "%s", val->u.sval );
    else if ( val->type==v_arr ) {
	putchar( '[' );
	if ( val->u.aval->argc>0 ) {
	    PrintVal(&val->u.aval->vals[0]);
	    for ( j=1; j<val->u.aval->argc; ++j ) {
		putchar(',');
		PrintVal(&val->u.aval->vals[j]);
	    }
	}
	putchar( ']' );
    } else if ( val->type==v_tag ) {
	printf( "'%c%c%c%c'", val->u.ival>>24, (val->u.ival>>16)&0xff, (val->u.ival>>8)&0xff, val->u.ival&0xff );
    } else if ( val->type==v_int )
	printf( "%d", val->u.ival );
    else if ( val->type==v_unicode )
	printf( "0u%x", val->u.ival );
    else if ( val->type==v_void )
	printf( "<void>");
    else
	printf( "<???>");
}

static void bPrint(Context *c) {
    int i;

    for ( i=1; i<c->a.argc; ++i )
	PrintVal(&c->a.vals[i] );
    printf( "\n" );
}

static void bError(Context *c) {

    if ( c->a.argc!=2 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str )
	error( c, "Expected string argument" );

    error( c, c->a.vals[1].u.sval );
}

static void bPostNotice(Context *c) {
    static const unichar_t format[] = { '%','.','2','0','0','h','s', '\0' };
    static const unichar_t notice[] = { 'A','t','t','e','n','t','i','o','n',  '\0' };

    if ( c->a.argc!=2 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str )
	error( c, "Expected string argument" );

    if ( screen_display==NULL )
	fprintf(stderr,"%s\n", c->a.vals[1].u.sval );
    else
	GWidgetPostNotice( notice, format, c->a.vals[1].u.sval );
}

static void bAskUser(Context *c) {
    char *quest, *def="";

    if ( c->a.argc!=2 && c->a.argc!=3 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str || ( c->a.argc==3 &&  c->a.vals[2].type!=v_str) )
	error( c, "Expected string argument" );
    quest = c->a.vals[1].u.sval;
    if ( c->a.argc==3 )
	def = c->a.vals[2].u.sval;
    if ( screen_display==NULL ) {
	char buffer[300];
	printf( "%s", quest );
	buffer[0] = '\0';
	c->return_val.type = v_str;
	if ( fgets(buffer,sizeof(buffer),stdin)==NULL ) {
	    clearerr(stdin);
	    c->return_val.u.sval = copy("");
	} else if ( buffer[0]=='\0' )
	    c->return_val.u.sval = copy(def);
	else
	    c->return_val.u.sval = copy(buffer);
    } else {
	unichar_t *t1, *t2, *ret;
	static unichar_t format[] = { '%','s', 0 };
	t1 = uc_copy(quest);
	ret = GWidgetAskString(t1,t2=uc_copy(def),format, t1);
	free(t1);
	free(t2);
	c->return_val.type = v_str;
	c->return_val.u.sval = cu_copy(ret);
	if ( ret==NULL )
	    c->return_val.u.sval = copy("");
	else
	    free(ret);
    }
}

static void bArray(Context *c) {
    int i;

    if ( c->a.argc!=2 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_int )
	error( c, "Expected integer argument" );
    else if ( c->a.vals[1].u.ival<=0 )
	error( c, "Argument must be positive" );
    c->return_val.type = v_arrfree;
    c->return_val.u.aval = galloc(sizeof(Array));
    c->return_val.u.aval->argc = c->a.vals[1].u.ival;
    c->return_val.u.aval->vals = galloc(c->a.vals[1].u.ival*sizeof(Val));
    for ( i=0; i<c->a.vals[1].u.ival; ++i )
	c->return_val.u.aval->vals[i].type = v_void;
}

static void bSizeOf(Context *c) {
    if ( c->a.argc!=2 )
	error( c, "Wrong number of arguments" );
    if ( c->a.vals[1].type!=v_arr && c->a.vals[1].type!=v_arrfree )
	error( c, "Expected array argument" );

    c->return_val.type = v_int;
    c->return_val.u.ival = c->a.vals[1].u.aval->argc;
}

static void bStrlen(Context *c) {

    if ( c->a.argc!=2 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str )
	error( c, "Bad type for argument" );

    c->return_val.type = v_int;
    c->return_val.u.ival = strlen( c->a.vals[1].u.sval );
}

static void bStrstr(Context *c) {
    char *pt;

    if ( c->a.argc!=3 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str || c->a.vals[2].type!=v_str )
	error( c, "Bad type for argument" );

    c->return_val.type = v_int;
    pt = strstr(c->a.vals[1].u.sval,c->a.vals[2].u.sval);
    c->return_val.u.ival = pt==NULL ? -1 : pt-c->a.vals[1].u.sval;
}

static void bStrcasestr(Context *c) {
    char *pt;

    if ( c->a.argc!=3 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str || c->a.vals[2].type!=v_str )
	error( c, "Bad type for argument" );

    c->return_val.type = v_int;
    pt = strstrmatch(c->a.vals[1].u.sval,c->a.vals[2].u.sval);
    c->return_val.u.ival = pt==NULL ? -1 : pt-c->a.vals[1].u.sval;
}

static void bStrrstr(Context *c) {
    char *pt;
    char *haystack, *needle;
    int nlen;

    if ( c->a.argc!=3 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str || c->a.vals[2].type!=v_str )
	error( c, "Bad type for argument" );

    c->return_val.type = v_int;
    haystack = c->a.vals[1].u.sval; needle = c->a.vals[2].u.sval;
    nlen = strlen( needle );
    for ( pt=haystack+strlen(haystack)-nlen; pt>=haystack; --pt )
	if ( strncmp(pt,needle,nlen)==0 )
    break;
    c->return_val.u.ival = pt-haystack;
}

static void bStrsub(Context *c) {
    int start, end;
    char *str;

    if ( c->a.argc!=3 && c->a.argc!=4 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str || c->a.vals[2].type!=v_int ||
	    (c->a.argc==4 && c->a.vals[3].type!=v_int))
	error( c, "Bad type for argument" );

    str = c->a.vals[1].u.sval;
    start = c->a.vals[2].u.ival;
    end = c->a.argc==4? c->a.vals[3].u.ival : strlen(str);
    if ( start<0 || start>strlen(str) || end<start || end>strlen(str) )
	error( c, "Arguments out of bounds" );
    c->return_val.type = v_str;
    c->return_val.u.sval = copyn(str+start,end-start);
}

static void bStrcasecmp(Context *c) {

    if ( c->a.argc!=3 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str || c->a.vals[2].type!=v_str )
	error( c, "Bad type for argument" );

    c->return_val.type = v_int;
    c->return_val.u.ival = strmatch(c->a.vals[1].u.sval,c->a.vals[2].u.sval);
}

static void bStrtol(Context *c) {
    int base = 10;

    if ( c->a.argc!=2 && c->a.argc!=3 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str || (c->a.argc==3 && c->a.vals[2].type!=v_int) )
	error( c, "Bad type for argument" );
    else if ( c->a.argc==3 ) {
	base = c->a.vals[2].u.ival;
	if ( base<0 || base==1 || base>36 )
	    error( c, "Argument out of bounds" );
    }

    c->return_val.type = v_int;
    c->return_val.u.ival = strtol(c->a.vals[1].u.sval,NULL,base);
}

static void bStrskipint(Context *c) {
    int base = 10;
    char *end;

    if ( c->a.argc!=2 && c->a.argc!=3 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str || (c->a.argc==3 && c->a.vals[2].type!=v_int) )
	error( c, "Bad type for argument" );
    else if ( c->a.argc==3 ) {
	base = c->a.vals[2].u.ival;
	if ( base<0 || base==1 || base>36 )
	    error( c, "Argument out of bounds" );
    }

    c->return_val.type = v_int;
    strtol(c->a.vals[1].u.sval,&end,base);
    c->return_val.u.ival = end-c->a.vals[1].u.sval;
}

#if 0
static void bGetPrefs(Context *c) {

    if ( c->a.argc!=2 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str )
	error( c, "Bad type for argument" );
    if ( !GetPrefs(c->a.vals[1].u.sval,&c->return_val) )
	errors( c, "Unknown Preference variable", c->a.vals[1].u.sval );
}

static void bSetPrefs(Context *c) {
    int ret;

    if ( c->a.argc!=3 && c->a.argc!=4 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str || (c->a.argc==4 && c->a.vals[3].type!=v_int) )
	error( c, "Bad type for argument" );
    if ( (ret=SetPrefs(c->a.vals[1].u.sval,&c->a.vals[2],c->a.argc==4?&c->a.vals[3]:NULL))==0 )
	errors( c, "Unknown Preference variable", c->a.vals[1].u.sval );
    else if ( ret==-1 )
	errors( c, "Bad type for preference variable",  c->a.vals[1].u.sval);
}
#endif

#if 0
static void bUnicodeFromName(Context *c) {
    if ( c->a.argc!=2 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str )
	error( c, "Bad type for argument" );
    c->return_val.type = v_int;
    c->return_val.u.ival = UniFromName(c->a.vals[1].u.sval);
}
#endif

static void bChr(Context *c) {
    char buf[2];
    char *temp;
    int i;

    if ( c->a.argc!=2 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type==v_int ) {
	if ( c->a.vals[1].u.ival<-128 || c->a.vals[1].u.ival>255 )
	    error( c, "Bad value for argument" );
	buf[0] = c->a.vals[1].u.ival; buf[1] = 0;
	c->return_val.type = v_str;
	c->return_val.u.sval = copy(buf);
    } else if ( c->a.vals[1].type==v_arr || c->a.vals[1].type==v_arrfree ) {
	Array *arr = c->a.vals[1].u.aval;
	temp = galloc((arr->argc+1)*sizeof(char));
	for ( i=0; i<arr->argc; ++i ) {
	    if ( arr->vals[i].type!=v_int )
		error( c, "Bad type for argument" );
	    else if ( c->a.vals[1].u.ival<-128 || c->a.vals[1].u.ival>255 )
		error( c, "Bad value for argument" );
	    temp[i] = arr->vals[i].u.ival;
	}
	temp[i] = 0;
	c->return_val.type = v_str;
	c->return_val.u.sval = temp;
    } else
	error( c, "Bad type for argument" );
}

static void bUtf8(Context *c) {
    int i;
#ifdef UNICHAR_16
    int32 buf[2];
    int32 *temp;
#else
    uint32 buf[2];
    uint32 *temp;
#endif

    if ( c->a.argc!=2 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type==v_int ) {
	if ( c->a.vals[1].u.ival<0 || c->a.vals[1].u.ival>0x10ffff )
	    error( c, "Bad value for argument" );
	buf[0] = c->a.vals[1].u.ival; buf[1] = 0;
	c->return_val.type = v_str;
#ifdef UNICHAR_16
	c->return_val.u.sval = u322utf8_copy(buf);
#else
	c->return_val.u.sval = u2utf8_copy(buf);
#endif
    } else if ( c->a.vals[1].type==v_arr || c->a.vals[1].type==v_arrfree ) {
	Array *arr = c->a.vals[1].u.aval;
	temp = galloc((arr->argc+1)*sizeof(uint32));
	for ( i=0; i<arr->argc; ++i ) {
	    if ( arr->vals[i].type!=v_int )
		error( c, "Bad type for argument" );
	    else if ( arr->vals[i].u.ival<0 || arr->vals[i].u.ival>0x10ffff )
		error( c, "Bad value for argument" );
	    temp[i] = arr->vals[i].u.ival;
	}
	temp[i] = 0;
	c->return_val.type = v_str;
#ifdef UNICHAR_16
	c->return_val.u.sval = u322utf8_copy(temp);
#else
	c->return_val.u.sval = u2utf8_copy(temp);
#endif
	free(temp);
    } else
	error( c, "Bad type for argument" );
}

static void bOrd(Context *c) {
    if ( c->a.argc!=2 && c->a.argc!=3 )
	error( c, "Wrong number of arguments" );
    else if ( c->a.vals[1].type!=v_str || ( c->a.argc==3 && c->a.vals[1].type!=v_int ))
	error( c, "Bad type for argument" );
    if ( c->a.argc==3 ) {
	if ( c->a.vals[2].u.ival<0 || c->a.vals[2].u.ival>strlen( c->a.vals[1].u.sval ))
	    error( c, "Bad value for argument" );
	c->return_val.type = v_int;
	c->return_val.u.ival = (uint8) c->a.vals[1].u.sval[c->a.vals[2].u.ival];
    } else {
	int i, len = strlen(c->a.vals[1].u.sval);
	c->return_val.type = v_arrfree;
	c->return_val.u.aval = galloc(sizeof(Array));
	c->return_val.u.aval->argc = len;
	c->return_val.u.aval->vals = galloc(len*sizeof(Val));
	for ( i=0; i<len; ++i ) {
	    c->return_val.u.aval->vals[i].type = v_int;
	    c->return_val.u.aval->vals[i].u.ival = (uint8) c->a.vals[1].u.sval[i];
	}
    }
}

/* **** File menu **** */

static void bQuit(Context *c) {
    if ( verbose>0 ) putchar('\n');
    if ( c->a.argc==1 )
exit(0);
    if ( c->a.argc>2 )
	error( c, "Too many arguments" );
    else if ( c->a.vals[1].type!=v_int )
	error( c, "Expected integer argument" );
    else
exit(c->a.vals[1].u.ival );
exit(1);
}

static TtfView *TFVAppend(TtfView *tfv) {
    /* Normally ttfviews get added to the tfv list when their windows are */
    /*  created. but we don't create any windows here, so... */
    TtfView *test;

    if ( tfv_list==NULL ) tfv_list = tfv;
    else {
	for ( test = tfv_list; test->next!=NULL; test=test->next );
	test->next = tfv;
    }
return( tfv );
}

static void bOpen(Context *c) {
    TtfFile *ttf;

    if ( c->a.argc!=2 )
	error( c, "Wrong number of arguments");
    else if ( c->a.vals[1].type!=v_str )
	error( c, "Open expects a filename" );
    ttf = LoadTtfFont(c->a.vals[1].u.sval);
    if ( ttf==NULL )
	errors(c, "Failed to open", c->a.vals[1].u.sval);
    if ( ttf->tfv!=NULL )
	/* All done */;
    else if ( screen_display!=NULL )
	TtfViewCreate(ttf);
    else
	TFVAppend(_TtfViewCreate(ttf));
    c->curtfv = ttf->tfv;
}

static void bClose(Context *c) {
    if ( c->a.argc!=1 )
	error( c, "Wrong number of arguments");
    if ( c->curtfv->gw!=NULL )
	GDrawDestroyWindow(c->curtfv->gw);
    else {
	if ( tfv_list==c->curtfv )
	    tfv_list = c->curtfv->next;
	else {
	    TtfView *n;
	    for ( n=tfv_list; n->next!=c->curtfv; n=n->next );
	    n->next = c->curtfv->next;
	}
	TtfViewFree(c->curtfv);
    }
    c->curtfv = NULL;
}

static void bSave(Context *c) {
    TtfFile *ttf = c->curtfv->ttf;

    if ( c->a.argc>2 )
	error( c, "Wrong number of arguments");
    if ( c->a.argc==2 ) {
	if ( c->a.vals[1].type!=v_str )
	    error(c,"If an argument is given to Save it must be a filename");
	if ( !TtfSave(ttf,c->a.vals[1].u.sval))
	    error(c,"Save As failed" );
    } else {
	if ( !TtfSave(ttf,ttf->filename) )
	    error(c,"Save failed" );
    }
}

static void bFontCount(Context *c) {

    if ( c->a.argc>1 )
	error( c, "Wrong number of arguments");
    c->return_val.type = v_int;
    c->return_val.u.ival = c->curtfv->ttf->font_cnt;
}

static void bFontName(Context *c) {
    TtfFile *ttf = c->curtfv->ttf;

    if ( c->a.argc>2 )
	error( c, "Wrong number of arguments");
    c->return_val.type = v_str;
    if ( c->a.argc==2 ) {
	if ( c->a.vals[1].type!=v_int )
	    error( c, "Bad type for argument" );
	if ( c->a.vals[1].u.ival<0 || c->a.vals[1].u.ival>=ttf->font_cnt )
	    error( c, "Font index out of range" );
	c->return_val.u.sval = cu_copy(ttf->fonts[c->a.vals[1].u.ival]->fontname);
    } else
	c->return_val.u.sval = cu_copy(ttf->fonts[c->curtfv->active_font]->fontname);
}

static void bGetCurrentFont(Context *c) {

    if ( c->a.argc>1 )
	error( c, "Wrong number of arguments");
    c->return_val.type = v_int;
    c->return_val.u.ival = c->curtfv->active_font;
}

static void bSetCurrentFont(Context *c) {
    TtfFile *ttf = c->curtfv->ttf;

    if ( c->a.argc!=2 )
	error( c, "Wrong number of arguments");
    if ( c->a.vals[1].type!=v_int )
	error( c, "Bad type for argument" );
    if ( c->a.vals[1].u.ival<0 || c->a.vals[1].u.ival>=ttf->font_cnt )
	error( c, "Font index out of range" );
    c->curtfv->active_font = c->a.vals[1].u.ival;
}

static void bTableCount(Context *c) {
    TtfFile *ttf = c->curtfv->ttf;

    if ( c->a.argc>2 )
	error( c, "Wrong number of arguments");
    c->return_val.type = v_int;
    if ( c->a.argc==2 ) {
	if ( c->a.vals[1].type!=v_int )
	    error( c, "Bad type for argument" );
	if ( c->a.vals[1].u.ival<0 || c->a.vals[1].u.ival>=ttf->font_cnt )
	    error( c, "Font index out of range" );
	c->return_val.u.ival = ttf->fonts[c->a.vals[1].u.ival]->tbl_cnt;
    } else
	c->return_val.u.ival = ttf->fonts[c->curtfv->active_font]->tbl_cnt;
}

static void bTableTag(Context *c) {
    TtfFile *ttf = c->curtfv->ttf;
    int fi;

    if ( c->a.argc!=2 && c->a.argc!=3 )
	error( c, "Wrong number of arguments");
    if ( c->a.argc==3 ) {
	if ( c->a.vals[2].type!=v_int )
	    error( c, "Bad type for argument" );
	if ( c->a.vals[2].u.ival<0 || c->a.vals[2].u.ival>=ttf->font_cnt )
	    error( c, "Font index out of range" );
	fi = c->a.vals[2].u.ival;
    } else
	fi = c->curtfv->active_font;
    if ( c->a.vals[1].type!=v_int )
	error( c, "Bad type for argument" );
    if ( c->a.vals[1].u.ival<0 || c->a.vals[1].u.ival>=ttf->fonts[fi]->tbl_cnt )
	error( c, "Table index out of range" );

    c->return_val.type = v_tag;
    c->return_val.u.ival = ttf->fonts[fi]->tbls[c->a.vals[1].u.ival]->name;
}

static void bTableIndex(Context *c) {
    TtfFile *ttf = c->curtfv->ttf;
    int fi, i;

    if ( c->a.argc!=2 && c->a.argc!=3 )
	error( c, "Wrong number of arguments");
    if ( c->a.argc==3 ) {
	if ( c->a.vals[2].type!=v_int )
	    error( c, "Bad type for argument" );
	if ( c->a.vals[2].u.ival<0 || c->a.vals[2].u.ival>=ttf->font_cnt )
	    error( c, "Font index out of range" );
	fi = c->a.vals[2].u.ival;
    } else
	fi = c->curtfv->active_font;
    if ( c->a.vals[1].type!=v_int && c->a.vals[1].type!=v_tag )
	error( c, "Bad type for argument" );
    for ( i=ttf->fonts[fi]->tbl_cnt-1; i>=0; --i )
	if ( ttf->fonts[fi]->tbls[i]->name==c->a.vals[1].u.ival )
    break;

    c->return_val.type = v_tag;
    c->return_val.u.ival = i;
}

static Table *findtable(Context *c,int base) {
    TtfFile *ttf = c->curtfv->ttf;
    int fi,i;

    fi = c->curtfv->active_font;
    if ( c->a.argc==base+1 ) {
	if ( c->a.vals[base].type!=v_int )
	    error( c, "Bad type for argument" );
	if ( c->a.vals[base].u.ival<0 || c->a.vals[base].u.ival>=ttf->font_cnt )
	    error( c, "Font index out of range" );
	fi = c->a.vals[base].u.ival;
    }
    if ( c->a.vals[base-1].type==v_int ) {
	if ( c->a.vals[base-1].u.ival<0 || c->a.vals[base-1].u.ival>=ttf->fonts[fi]->tbl_cnt )
	    error( c, "Table index out of range" );
return( ttf->fonts[fi]->tbls[c->a.vals[base-1].u.ival] );
    } else if ( c->a.vals[base-1].type==v_tag ) {
	for ( i=ttf->fonts[fi]->tbl_cnt-1; i>=0; --i )
	    if ( ttf->fonts[fi]->tbls[i]->name==c->a.vals[base-1].u.ival )
	break;
	if ( i<0 )
	    error( c,"Table not found in font" );
return( ttf->fonts[fi]->tbls[i] );
    } else
	error( c, "Bad type for argument" );
return( NULL );
}

static void namesearch(Context *c,char *str,Table *tbl, Val *ret) {
    int i;
    struct tabnames *names;

    for ( i=0 ; tabnamelist[i].tag!=0 && tabnamelist[i].tag!=tbl->name; ++i );
    if ( tabnamelist[i].tag==0 )
	error( c,"mensis does not support names in this table" );
    names = tabnamelist[i].tabnames;
    for ( i=0; names[i].name!=NULL && strcmp(names[i].name,str)!=0; ++i );
    if ( names[i].name==NULL )
	errors( c,"This table does not contain the following name", str );
    if ( names[i].offset+(names[i].type == v_ttf32pt ? 3 : names[i].type == v_ttf16pt ? 1 : 0)>=
	    tbl->newlen )
	errors( c,"This version of the table is too small to contain", str );

    ret->type = names[i].type;
    TableFillup(tbl);
    ret->u.ttfptval = tbl->data + names[i].offset;
}

static void bGetTableField(Context *c) {
    Table *tbl;
    Val temp;

    if ( c->a.argc!=3 && c->a.argc!=4 )
	error( c, "Wrong number of arguments");
    tbl = findtable(c,3);

    if ( c->a.vals[1].type!=v_str )
	error( c, "Bad type for argument" );
    namesearch(c,c->a.vals[1].u.sval,tbl,&temp);

    c->return_val.type = v_int;
    c->return_val.u.ival = derefttfpt(c,&temp);
}

static void bSetTableField(Context *c) {
    Table *tbl;
    Val temp;

    if ( c->a.argc!=4 && c->a.argc!=5 )
	error( c, "Wrong number of arguments");
    tbl = findtable(c,4);

    if ( c->a.vals[1].type!=v_str ||
	    ( c->a.vals[2].type!=v_int && c->a.vals[2].type!=v_tag ))
	error( c, "Bad type for argument" );
    namesearch(c,c->a.vals[1].u.sval,tbl,&temp);
    setttfpt(c,&temp,c->a.vals[2].u.ival);
}

static void bGetTableOffset(Context *c) {
    Table *tbl;
    Val temp;

    if ( c->a.argc!=4 && c->a.argc!=5 )
	error( c, "Wrong number of arguments");
    tbl = findtable(c,4);

    if ( c->a.vals[1].type!=v_int || c->a.vals[2].type!=v_int )
	error( c, "Bad type for argument" );
    if ( c->a.vals[2].u.ival!=1 && c->a.vals[2].u.ival!=2 && c->a.vals[2].u.ival!=4 )
	error( c, "Bad value for size, must be 1, 2, or 4" );
    if ( c->a.vals[1].u.ival<0 || c->a.vals[1].u.ival+c->a.vals[2].u.ival-1>=tbl->newlen )
	error( c,"Offset points outside of table" );

    TableFillup(tbl);
    temp.u.ttfptval = tbl->data+c->a.vals[1].u.ival;
    temp.type = c->a.vals[2].u.ival==1 ? v_ttf8pt :
		c->a.vals[2].u.ival==2 ? v_ttf16pt : v_ttf32pt;

    c->return_val.type = v_int;
    c->return_val.u.ival = derefttfpt(c,&temp);
}

static void bSetTableOffset(Context *c) {
    Table *tbl;
    Val temp;

    if ( c->a.argc!=5 && c->a.argc!=6 )
	error( c, "Wrong number of arguments");
    tbl = findtable(c,5);

    if ( c->a.vals[1].type!=v_int || c->a.vals[2].type!=v_int ||
	    ( c->a.vals[3].type!=v_int && c->a.vals[3].type!=v_tag ))
	error( c, "Bad type for argument" );
    if ( c->a.vals[2].u.ival!=1 && c->a.vals[2].u.ival!=2 && c->a.vals[2].u.ival!=4 )
	error( c, "Bad value for size, must be 1, 2, or 4" );
    if ( c->a.vals[1].u.ival<0 || c->a.vals[1].u.ival+c->a.vals[2].u.ival-1>=tbl->newlen )
	error( c,"Offset points outside of table" );

    TableFillup(tbl);
    temp.u.ttfptval = tbl->data+c->a.vals[1].u.ival;
    temp.type = c->a.vals[2].u.ival==1 ? v_ttf8pt :
		 c->a.vals[2].u.ival==2 ? v_ttf16pt : v_ttf32pt;
    setttfpt(c,&temp,c->a.vals[3].u.ival);
}


static struct builtins { char *name; void (*func)(Context *); int nofontok; } builtins[] = {
/* Generic utilities */
    { "Print", bPrint, 1 },
    { "Error", bError, 1 },
    { "AskUser", bAskUser, 1 },
    { "PostNotice", bPostNotice, 1 },
    { "Array", bArray, 1 },
    { "SizeOf", bSizeOf, 1 },
    { "Strsub", bStrsub, 1 },
    { "Strlen", bStrlen, 1 },
    { "Strstr", bStrstr, 1 },
    { "Strrstr", bStrrstr, 1 },
    { "Strcasestr", bStrcasestr, 1 },
    { "Strcasecmp", bStrcasecmp, 1 },
    { "Strtol", bStrtol, 1 },
    { "Strskipint", bStrskipint, 1 },
#if 0
    { "GetPref", bGetPrefs, 1 },
    { "SetPref", bSetPrefs, 1 },
    { "UnicodeFromName", bUnicodeFromName, 1 },
#endif
    { "Chr", bChr, 1 },
    { "Ord", bOrd, 1 },
    { "Utf8", bUtf8, 1 },
/* File menu */
    { "Quit", bQuit, 1 },
    { "Open", bOpen, 1 },
    { "Close", bClose },
    { "Save", bSave },
/* Useful stuff */
    { "FontCount", bFontCount },
    { "FontName", bFontName },
    { "SetCurrentFont", bSetCurrentFont },
    { "GetCurrentFont", bGetCurrentFont },
    { "TableCount", bTableCount },
    { "TableTag", bTableTag },
    { "TableIndex", bTableIndex },
    { "SetTableField", bSetTableField },
    { "GetTableField", bGetTableField },
    { "SetTableOffset", bSetTableOffset },
    { "GetTableOffset", bGetTableOffset },
    { NULL }
};

/* ******************************* Interpreter ****************************** */

static void expr(Context*,Val *val);
static void statement(Context*);

static int cgetc(Context *c) {
    int ch;
    if ( c->ungotch ) {
	ch = c->ungotch;
	c->ungotch = 0;
return( ch );
    }
    ch = getc(c->script);
    if ( verbose>0 )
	putchar(ch);
    if ( ch=='\r' ) {
	int nch = getc(c->script);
	if ( nch!='\n' )
	    ungetc(nch,c->script);
	else if ( verbose>0 )
	    putchar('\n');
	++c->lineno;
    } else if ( ch=='\n' )
	++c->lineno;
return( ch );
}

static void cungetc(int ch,Context *c) {
    if ( c->ungotch )
	fprintf( stderr, "Internal error: Attempt to unget two characters\n" );
    c->ungotch = ch;
}

static long ctell(Context *c) {
    long pos = ftell(c->script);
    if ( c->ungotch )
	--pos;
return( pos );
}

static void cseek(Context *c,long pos) {
    fseek(c->script,pos,SEEK_SET);
    c->ungotch = 0;
    c->backedup = false;
}

static enum token_type NextToken(Context *c) {
    int ch, peek;
    enum token_type tok = tt_error;

    if ( c->backedup ) {
	c->backedup = false;
return( c->tok );
    }
    do {
	ch = cgetc(c);
	if ( ch=='.' ) {peek = cgetc(c); cungetc(peek,c); }
	if ( isalpha(ch) || ch=='$' || ch=='_' || ch=='@' ) {
	    char *pt = c->tok_text, *end = c->tok_text+TOK_MAX;
	    int toolong = false;
	    while ( (isalnum(ch) || ch=='$' || ch=='_' || ch=='.' || ch=='@' ) && pt<end ) {
		*pt++ = ch;
		ch = cgetc(c);
	    }
	    *pt = '\0';
	    while ( isalnum(ch) || ch=='$' || ch=='_' || ch=='.' ) {
		ch = cgetc(c);
		toolong = true;
	    }
	    cungetc(ch,c);
	    tok = tt_name;
	    if ( toolong )
		error( c, "Name too long" );
	    else {
		int i;
		for ( i=0; keywords[i].name!=NULL; ++i )
		    if ( strcmp(keywords[i].name,c->tok_text)==0 ) {
			tok = keywords[i].tok;
		break;
		    }
	    }
	} else if ( isdigit(ch) || (ch=='.' && isdigit(peek)) ) {
	    int val=0;
	    tok = tt_number;
	    if ( ch!='0' ) {
		while ( isdigit(ch)) {
		    val = 10*val+(ch-'0');
		    ch = cgetc(c);
		}
	    } else if ( isdigit(ch=cgetc(c)) ) {
		while ( isdigit(ch) && ch<'8' ) {
		    val = 8*val+(ch-'0');
		    ch = cgetc(c);
		}
	    } else if ( ch=='X' || ch=='x' || ch=='u' || ch=='U' ) {
		if ( ch=='u' || ch=='U' ) tok = tt_unicode;
		ch = cgetc(c);
		while ( isdigit(ch) || (ch>='a' && ch<='f') || (ch>='A'&&ch<='F')) {
		    if ( isdigit(ch))
			ch -= '0';
		    else if ( ch>='a' && ch<='f' )
			ch += 10-'a';
		    else
			ch += 10-'A';
		    val = 16*val+ch;
		    ch = cgetc(c);
		}
	    }
	    cungetc(ch,c);
	    c->tok_val.u.ival = val;
	    c->tok_val.type = tok==tt_number ? v_int : v_unicode;
	} else if ( ch=='\'' || ch=='"' ) {
	    int quote = ch;
	    char *pt = c->tok_text, *end = c->tok_text+TOK_MAX;
	    int toolong = false;
	    ch = cgetc(c);
	    while ( ch!=EOF && ch!='\r' && ch!='\n' && ch!=quote ) {
		if ( ch=='\\' ) {
		    ch=cgetc(c);
		    if ( ch=='\n' || ch=='\r' ) {
			cungetc(ch,c);
			ch = '\\';
		    } else if ( ch==EOF )
			ch = '\\';
		}
		if ( pt<end )
		    *pt++ = ch;
		else
		    toolong = true;
		ch = cgetc(c);
	    }
	    *pt = '\0';
	    if ( ch=='\n' || ch=='\r' )
		cungetc(ch,c);
	    if ( quote=='\'' ) {
		if ( pt-c->tok_text != 4 )
		    error( c, "Tags must be 4 characters long" );
		tok = tt_tag;
		pt = c->tok_text;
		c->tok_val.u.ival = (pt[0]<<24)|(pt[1]<<16)|(pt[2]<<8)|pt[3];
		c->tok_val.type = v_tag;
	    } else {
		tok = tt_string;
		if ( toolong )
		    error( c, "String too long" );
	    }
	} else switch( ch ) {
	  case EOF:
	    tok = tt_eof;
	  break;
	  case ' ': case '\t':
	    /* Ignore spaces */
	  break;
	  case '#':
	    /* Ignore comments */
	    while ( (ch=cgetc(c))!=EOF && ch!='\r' && ch!='\n' );
	    if ( ch=='\r' || ch=='\n' )
		cungetc(ch,c);
	  break;
	  case '.':
	    tok = tt_dot;
	  break;
	  case '(':
	    tok = tt_lparen;
	  break;
	  case ')':
	    tok = tt_rparen;
	  break;
	  case '[':
	    tok = tt_lbracket;
	  break;
	  case ']':
	    tok = tt_rbracket;
	  break;
	  case ',':
	    tok = tt_comma;
	  break;
	  case ':':
	    tok = tt_colon;
	  break;
	  case ';': case '\n': case '\r':
	    tok = tt_eos;
	  break;
	  case '-':
	    tok = tt_minus;
	    ch=cgetc(c);
	    if ( ch=='=' )
		tok = tt_minuseq;
	    else if ( ch=='-' )
		tok = tt_decr;
	    else
		cungetc(ch,c);
	  break;
	  case '+':
	    tok = tt_plus;
	    ch=cgetc(c);
	    if ( ch=='=' )
		tok = tt_pluseq;
	    else if ( ch=='+' )
		tok = tt_incr;
	    else
		cungetc(ch,c);
	  break;
	  case '!':
	    tok = tt_not;
	    ch=cgetc(c);
	    if ( ch=='=' )
		tok = tt_ne;
	    else
		cungetc(ch,c);
	  break;
	  case '~':
	    tok = tt_bitnot;
	  break;
	  case '*':
	    tok = tt_mul;
	    ch=cgetc(c);
	    if ( ch=='=' )
		tok = tt_muleq;
	    else
		cungetc(ch,c);
	  break;
	  case '%':
	    tok = tt_mod;
	    ch=cgetc(c);
	    if ( ch=='=' )
		tok = tt_modeq;
	    else
		cungetc(ch,c);
	  break;
	  case '/':
	    ch=cgetc(c);
	    if ( ch=='/' ) {
		/* another comment to eol */;
		while ( (ch=cgetc(c))!=EOF && ch!='\r' && ch!='\n' );
		if ( ch=='\r' || ch=='\n' )
		    cungetc(ch,c);
	    } else if ( ch=='*' ) {
		int found=false;
		ch = cgetc(c);
		while ( !found || ch!='/' ) {
		    if ( ch==EOF )
		break;
		    if ( ch=='*' ) found = true;
		    else found = false;
		    ch = cgetc(c);
		}
	    } else if ( ch=='=' ) {
		tok = tt_diveq;
	    } else {
		tok = tt_div;
		cungetc(ch,c);
	    }
	  break;
	  case '&':
	    tok = tt_bitand;
	    ch=cgetc(c);
	    if ( ch=='&' )
		tok = tt_and;
	    else
		cungetc(ch,c);
	  break;
	  case '|':
	    tok = tt_bitor;
	    ch=cgetc(c);
	    if ( ch=='|' )
		tok = tt_or;
	    else
		cungetc(ch,c);
	  break;
	  case '^':
	    tok = tt_xor;
	  break;
	  case '=':
	    tok = tt_assign;
	    ch=cgetc(c);
	    if ( ch=='=' )
		tok = tt_eq;
	    else
		cungetc(ch,c);
	  break;
	  case '>':
	    tok = tt_gt;
	    ch=cgetc(c);
	    if ( ch=='=' )
		tok = tt_ge;
	    else
		cungetc(ch,c);
	  break;
	  case '<':
	    tok = tt_lt;
	    ch=cgetc(c);
	    if ( ch=='=' )
		tok = tt_le;
	    else
		cungetc(ch,c);
	  break;
	  default:
	    fprintf( stderr, "%s:%d Unexpected character %c (%d)\n",
		    c->filename, c->lineno, ch, ch);
	    traceback(c);
	}
    } while ( tok==tt_error );

    c->tok = tok;
return( tok );
}

static void backuptok(Context *c) {
    if ( c->backedup )
	fprintf( stderr, "%s:%d Internal Error: Attempt to back token twice\n",
		c->filename, c->lineno );
    c->backedup = true;
}

#define PE_ARG_MAX	25

static void docall(Context *c,char *name,Val *val) {
    /* Be prepared for c->donteval */
    Val args[PE_ARG_MAX];
    Array *dontfree[PE_ARG_MAX];
    int i;
    enum token_type tok;
    Context sub;

    tok = NextToken(c);
    dontfree[0] = NULL;
    if ( tok==tt_rparen )
	i = 1;
    else {
	backuptok(c);
	for ( i=1; tok!=tt_rparen; ++i ) {
	    if ( i>=PE_ARG_MAX )
		error(c,"Too many arguments");
	    expr(c,&args[i]);
	    tok = NextToken(c);
	    if ( tok!=tt_comma )
		expect(c,tt_rparen,tok);
	    dontfree[i]=NULL;
	}
    }

    if ( !c->donteval ) {
	args[0].type = v_str;
	args[0].u.sval = name;
	memset( &sub,0,sizeof(sub));
	sub.caller = c;
	sub.a.vals = args;
	sub.a.argc = i;
	sub.return_val.type = v_void;
	sub.filename = name;
	sub.curtfv = c->curtfv;
	sub.trace = c->trace;
	sub.dontfree = dontfree;
	for ( i=0; i<sub.a.argc; ++i ) {
	    dereflvalif(c,&args[i]);
	    if ( args[i].type == v_arrfree )
		args[i].type = v_arr;
	    else if ( args[i].type == v_arr )
		dontfree[i] = args[i].u.aval;
	}

	if ( c->trace.u.ival ) {
	    printf( "%s:%d Calling %s(", GFileNameTail(c->filename), c->lineno,
		    name );
	    for ( i=1; i<sub.a.argc; ++i ) {
		if ( i!=1 ) putchar(',');
		if ( args[i].type == v_int )
		    printf( "%d", args[i].u.ival );
		else if ( args[i].type == v_unicode )
		    printf( "0u%x", args[i].u.ival );
		else if ( args[i].type == v_str )
		    printf( "\"%s\"", args[i].u.sval );
		else if ( args[i].type == v_void )
		    printf( "<void>");
		else
		    printf( "<???>");
	    }
	    printf(")\n");
	}

	for ( i=0; builtins[i].name!=NULL; ++i )
	    if ( strcmp(builtins[i].name,name)==0 )
	break;
	if ( builtins[i].name!=NULL ) {
	    if ( sub.curtfv==NULL && !builtins[i].nofontok )
		error(&sub,"This command requires an active font");
	    (builtins[i].func)(&sub);
	} else {
	    if ( strchr(name,'/')==NULL && strchr(c->filename,'/')!=NULL ) {
		char *pt;
		sub.filename = galloc(strlen(c->filename)+strlen(name)+1);
		strcpy(sub.filename,c->filename);
		pt = strrchr(sub.filename,'/');
		strcpy(pt+1,name);
	    }
	    sub.script = fopen(sub.filename,"r");
	    if ( sub.script==NULL )
		error(&sub, "No such script-file or buildin function");
	    else {
		sub.lineno = 1;
		while ( !sub.returned && (tok = NextToken(&sub))!=tt_eof ) {
		    backuptok(&sub);
		    statement(&sub);
		}
		fclose(sub.script); sub.script = NULL;
	    }
	    if ( sub.filename!=name )
		free( sub.filename );
	}
	c->curtfv = sub.curtfv;
    }
    calldatafree(&sub);
    if ( val->type==v_str )
	free(val->u.sval);
    *val = sub.return_val;
}

static void handlename(Context *c,Val *val) {
    char name[TOK_MAX+1];
    enum token_type tok;
    int temp;
    char *pt;
    TtfFile *ttf;

    strcpy(name,c->tok_text);
    val->type = v_void;
    tok = NextToken(c);
    if ( tok==tt_lparen ) {
	docall(c,name,val);
    } else if ( c->donteval ) {
	backuptok(c);
    } else {
	if ( *name=='$' ) {
	    if ( isdigit(name[1])) {
		temp = 0;
		for ( pt = name+1; isdigit(*pt); ++pt )
		    temp = 10*temp+*pt-'0';
		if ( *pt=='\0' && temp<c->a.argc ) {
		    val->type = v_lval;
		    val->u.lval = &c->a.vals[temp];
		}
	    } else if ( strcmp(name,"$argc")==0 || strcmp(name,"$#")==0 ) {
		val->type = v_int;
		val->u.ival = c->a.argc;
	    } else if ( strcmp(name,"$argv")==0 ) {
		val->type = v_arr;
		val->u.aval = &c->a;
	    } else if ( strcmp(name,"$curfont")==0 || strcmp(name,"$nextfont")==0 ||
		    strcmp(name,"$firstfont")==0 ) {
		if ( strcmp(name,"$firstfont")==0 ) {
		    if ( tfv_list==NULL ) ttf=NULL;
		    else ttf = tfv_list->ttf;
		} else {
		    if ( c->curtfv==NULL ) error(c,"No current font");
		    if ( strcmp(name,"$curfont")==0 ) 
			ttf = c->curtfv->ttf;
		    else {
			if ( c->curtfv->next==NULL ) ttf = NULL;
			else ttf = c->curtfv->next->ttf;
		    }
		}
		val->type = v_str;
		val->u.sval = copy(ttf==NULL?"":ttf->filename);
	    } else if ( strcmp(name,"$fontname")==0 ) {
		if ( c->curtfv==NULL ) error(c,"No current font");
		val->type = v_str;
		val->u.sval = cu_copy(c->curtfv->ttf->fonts[c->curtfv->active_font]->fontname);
	    } else if ( strcmp(name,"$fontchanged")==0 ) {
		int i;
		if ( c->curtfv==NULL ) error(c,"No current font");
		val->type = v_int;
		val->u.ival = 0;
		for ( i=0; i<c->curtfv->ttf->fonts[c->curtfv->active_font]->tbl_cnt; ++i )
		    if ( c->curtfv->ttf->fonts[c->curtfv->active_font]->tbls[i]->changed ) {
			val->u.ival = 1;
		break;
		    }
	    } else if ( strcmp(name,"$trace")==0 ) {
		val->type = v_lval;
		val->u.lval = &c->trace;
	    } else if ( strcmp(name,"$version")==0 ) {
		extern const char *source_version_str;
		val->type = v_str;
		val->u.sval = copy(source_version_str);
#if 0
	    } else if ( GetPrefs(name+1,val)) {
		/* Done */
#endif
	    }
	} else if ( *name=='@' ) {
	    if ( c->curtfv==NULL ) error(c,"No current font");
	    DicaLookup(c->curtfv->fontvars,name,val);
	} else if ( *name=='_' ) {
	    DicaLookup(&globals,name,val);
	} else {
	    DicaLookup(&c->locals,name,val);
	}
	if ( tok==tt_assign && val->type==v_void && *name!='$' ) {
	    /* It's ok to create this as a new variable, we're going to assign to it */
	    if ( *name=='@' ) {
		if ( c->curtfv->fontvars==NULL )
		    c->curtfv->fontvars = gcalloc(1,sizeof(struct dictionary));
		DicaNewEntry(c->curtfv->fontvars,name,val);
	    } else if ( *name=='_' ) {
		DicaNewEntry(&globals,name,val);
	    } else {
		DicaNewEntry(&c->locals,name,val);
	    }
	}
	if ( val->type==v_void )
	    errors(c, "Undefined variable", name);
	backuptok(c);
    }
}

static void IndexTable(Context *c,Val *val) {
    int i, size;
    TtfFile *ttf;
    TtfFont *t;
    Val temp;
    enum token_type tok;

    if ( c->curtfv==NULL ) error(c,"No current font");

    ttf = c->curtfv->ttf;
    t = ttf->fonts[c->curtfv->active_font];
    for ( i=t->tbl_cnt-1; i>=0 ; --i )
	if ( t->tbls[i]->name==val->u.ival )
    break;
#if 0
    if ( i<0 ) {
	for ( j=0; j<ttf->font_cnt; ++j ) {
	    t = ttf->fonts[j];
	    for ( i=t->tbl_cnt-1; i>=0 ; --i )
		if ( t->tbls[i]->name==val->u.ival )
	    break;
	    if ( i>=0 )
	break;
	}
    }
#endif
    if ( i<0 ) error(c,"Requested table not in file");

    expr(c,&temp);
    tok=NextToken(c);
    size = 1;
    if ( tok==tt_colon ) {
	tok=NextToken(c);
	expect(c,tt_name,tok);
	if ( strcmp(c->tok_text,"s")==0 )
	    size = 2;
	else if ( strcmp(c->tok_text,"i")==0 )
	    size = 4;
	else if ( strcmp(c->tok_text,"c")==0 )
	    size = 1;
	else
	    error( c,"Illegal size modifier in table index (must be :s, :i or :c)" );
	tok=NextToken(c);
    }
    expect(c,tt_rbracket,tok);

    if ( !c->donteval ) {
	if ( temp.type!=v_int ) error( c,"Attempt to index a table with an expression of the wrong type" );
	if ( temp.u.ival+size-1>=t->tbls[i]->newlen || temp.u.ival<0 )
	    error( c,"Index into a table out of bounds" );
	val->type = size==1 ? v_ttf8pt : size==2 ? v_ttf16pt : v_ttf32pt;
	TableFillup(t->tbls[i]);
	val->u.ttfptval = t->tbls[i]->data + temp.u.ival;
    }
}

static void DotTable(Context *c,Val *val) {
    int i;
    TtfFile *ttf;
    TtfFont *t;
    enum token_type tok;

    if ( c->curtfv==NULL ) error(c,"No current font");
    tok = NextToken(c);
    expect(c,tt_name,tok);

    ttf = c->curtfv->ttf;
    t = ttf->fonts[c->curtfv->active_font];
    for ( i=t->tbl_cnt-1; i>=0 ; --i )
	if ( t->tbls[i]->name==val->u.ival )
    break;
#if 0
    if ( i<0 ) {
	for ( j=0; j<ttf->font_cnt; ++j ) {
	    t = ttf->fonts[j];
	    for ( i=t->tbl_cnt-1; i>=0 ; --i )
		if ( t->tbls[i]->name==val->u.ival )
	    break;
	    if ( i>=0 )
	break;
	}
    }
#endif
    if ( i<0 ) error(c,"Requested table not in file");

    namesearch(c,c->tok_text,t->tbls[i],val);
}

static void IncrDecr(Context *c,Val *val, enum token_type tok) {
    if ( !c->donteval ) {
	if ( val->type==v_lval ) {
	    if ( val->u.lval->type!=v_int && val->u.lval->type!=v_unicode )
		error( c, "Invalid type in integer expression" );
	    if ( tok == tt_incr )
		++val->u.lval->u.ival;
	    else
		--val->u.lval->u.ival;
	    dereflvalif(c,val);
	} else if ( val->type==v_ttf8pt || val->type==v_ttf16pt || val->type==v_ttf32pt ) {
	    int temp = derefttfpt(c,val);
	    if ( tok == tt_incr )
		++temp;
	    else
		--temp;
	    setttfpt(c,val,temp);
	    val->type = v_int;
	    val->u.ival = temp;
	} else 
	    error( c, "Expected lvalue" );
    }
}

static void term(Context *c,Val *val) {
    enum token_type tok = NextToken(c);
    Val temp;

    if ( tok==tt_lparen ) {
	expr(c,val);
	tok = NextToken(c);
	expect(c,tt_rparen,tok);
    } else if ( tok==tt_number || tok==tt_unicode ) {
	*val = c->tok_val;
    } else if ( tok==tt_tag ) {
	*val = c->tok_val;
	tok = NextToken(c);
	if ( tok==tt_dot )
	    DotTable(c,val);
	else if ( tok==tt_lbracket )
	    IndexTable(c,val);
	else
	    backuptok(c);
    } else if ( tok==tt_string ) {
	val->type = v_str;
	val->u.sval = copy( c->tok_text );
    } else if ( tok==tt_name ) {
	handlename(c,val);
    } else if ( tok==tt_minus || tok==tt_plus || tok==tt_not || tok==tt_bitnot ) {
	term(c,val);
	if ( !c->donteval ) {
	    dereflvalif(c,val);
	    if ( val->type!=v_int )
		error( c, "Invalid type in integer expression" );
	    if ( tok==tt_minus )
		val->u.ival = -val->u.ival;
	    else if ( tok==tt_not )
		val->u.ival = !val->u.ival;
	    else if ( tok==tt_bitnot )
		val->u.ival = ~val->u.ival;
	}
    } else if ( tok==tt_incr || tok==tt_decr ) {
	term(c,val);
	IncrDecr(c,val,tok);
    } else
	expect(c,tt_name,tok);

    tok = NextToken(c);
    while ( tok==tt_incr || tok==tt_decr || tok==tt_colon || tok==tt_lparen || tok==tt_lbracket ) {
	if ( tok==tt_colon ) {
	    if ( c->donteval ) {
		tok = NextToken(c);
		expect(c,tt_name,tok);
	    } else {
		dereflvalif(c,val);
		if ( val->type!=v_str )
		    error( c, "Invalid type in string expression" );
		else {
		    char *pt, *ept;
		    tok = NextToken(c);
		    expect(c,tt_name,tok);
		    if ( strcmp(c->tok_text,"h")==0 ) {
			pt = strrchr(val->u.sval,'/');
			if ( pt!=NULL ) *pt = '\0';
		    } else if ( strcmp(c->tok_text,"t")==0 ) {
			pt = strrchr(val->u.sval,'/');
			if ( pt!=NULL ) {
			    char *ret = copy(pt+1);
			    free(val->u.sval);
			    val->u.sval = ret;
			}
		    } else if ( strcmp(c->tok_text,"r")==0 ) {
			pt = strrchr(val->u.sval,'/');
			if ( pt==NULL ) pt=val->u.sval;
			ept = strrchr(pt,'.');
			if ( ept!=NULL ) *ept = '\0';
		    } else if ( strcmp(c->tok_text,"e")==0 ) {
			pt = strrchr(val->u.sval,'/');
			if ( pt==NULL ) pt=val->u.sval;
			ept = strrchr(pt,'.');
			if ( ept!=NULL ) {
			    char *ret = copy(ept+1);
			    free(val->u.sval);
			    val->u.sval = ret;
			}
		    } else
			errors(c,"Unknown colon substitution", c->tok_text );
		}
	    }
	} else if ( tok==tt_lparen ) {
	    if ( c->donteval ) {
		docall(c,NULL,val);
	    } else {
		dereflvalif(c,val);
		if ( val->type!=v_str ) {
		    error(c,"Expected string to hold filename in procedure call");
		} else
		    docall(c,val->u.sval,val);
	    }
	} else if ( tok==tt_lbracket ) {
	    expr(c,&temp);
	    tok = NextToken(c);
	    expect(c,tt_rbracket,tok);
	    if ( !c->donteval ) {
		dereflvalif(c,&temp);
		if ( val->type==v_lval && (val->u.lval->type==v_arr ||val->u.lval->type==v_arrfree))
		    *val = *val->u.lval;
		if ( val->type!=v_arr && val->type!=v_arrfree )
		    error(c,"Array required");
		if (temp.type!=v_int )
		    error(c,"Integer expression required in array");
		else if ( temp.u.ival<0 || temp.u.ival>=val->u.aval->argc )
		    error(c,"Integer expression out of bounds in array");
		else if ( val->type==v_arrfree ) {
		    temp = val->u.aval->vals[temp.u.ival];
		    arrayfree(val->u.aval);
		    *val = temp;
		} else {
		    val->type = v_lval;
		    val->u.lval = &val->u.aval->vals[temp.u.ival];
		}
	    }
	} else if ( !c->donteval ) {
	    IncrDecr(c,val,tok);
	}
	tok = NextToken(c);
    }
    backuptok(c);
}

static void mul(Context *c,Val *val) {
    Val other;
    enum token_type tok;

    term(c,val);
    tok = NextToken(c);
    while ( tok==tt_mul || tok==tt_div || tok==tt_mod ) {
	other.type = v_void;
	term(c,&other);
	if ( !c->donteval ) {
	    dereflvalif(c,val);
	    dereflvalif(c,&other);
	    if ( val->type!=v_int || other.type!=v_int )
		error( c, "Invalid type in integer expression" );
	    else if ( (tok==tt_div || tok==tt_mod ) && other.u.ival==0 )
		error( c, "Division by zero" );
	    else if ( tok==tt_mul )
		val->u.ival *= other.u.ival;
	    else if ( tok==tt_mod )
		val->u.ival %= other.u.ival;
	    else
		val->u.ival /= other.u.ival;
	}
	tok = NextToken(c);
    }
    backuptok(c);
}

static void add(Context *c,Val *val) {
    Val other;
    enum token_type tok;

    mul(c,val);
    tok = NextToken(c);
    while ( tok==tt_plus || tok==tt_minus ) {
	other.type = v_void;
	mul(c,&other);
	if ( !c->donteval ) {
	    dereflvalif(c,val);
	    dereflvalif(c,&other);
	    if ( val->type==v_str && (other.type==v_str || other.type==v_int) && tok==tt_plus ) {
		char *ret, *temp;
		char buffer[10];
		if ( other.type == v_int ) {
		    sprintf(buffer,"%d", other.u.ival);
		    temp = buffer;
		} else
		    temp = other.u.sval;
		ret = galloc(strlen(val->u.sval)+strlen(temp)+1);
		strcpy(ret,val->u.sval);
		strcat(ret,temp);
		if ( other.type==v_str ) free(other.u.sval);
		free(val->u.sval);
		val->u.sval = ret;
	    } else if ( (val->type!=v_int && val->type!=v_unicode) || (other.type!=v_int&&other.type!=v_unicode) )
		error( c, "Invalid type in integer expression" );
	    else if ( tok==tt_plus )
		val->u.ival += other.u.ival;
	    else
		val->u.ival -= other.u.ival;
	}
	tok = NextToken(c);
    }
    backuptok(c);
}

static void comp(Context *c,Val *val) {
    Val other;
    int cmp;
    enum token_type tok;

    add(c,val);
    tok = NextToken(c);
    while ( tok==tt_eq || tok==tt_ne || tok==tt_gt || tok==tt_lt || tok==tt_ge || tok==tt_le ) {
	other.type = v_void;
	add(c,&other);
	if ( !c->donteval ) {
	    dereflvalif(c,val);
	    dereflvalif(c,&other);
	    if ( val->type==v_str && other.type==v_str ) {
		cmp = strcmp(val->u.sval,other.u.sval);
		free(val->u.sval); free(other.u.sval);
	    } else if (( val->type==v_int || val->type==v_unicode || val->type==v_tag) &&
		    (other.type==v_int || other.type==v_unicode || other.type==v_tag)) {
		cmp = val->u.ival - other.u.ival;
	    } else 
		error( c, "Invalid type in integer expression" );
	    val->type = v_int;
	    if ( tok==tt_eq ) val->u.ival = (cmp==0);
	    else if ( tok==tt_ne ) val->u.ival = (cmp!=0);
	    else if ( tok==tt_gt ) val->u.ival = (cmp>0);
	    else if ( tok==tt_lt ) val->u.ival = (cmp<0);
	    else if ( tok==tt_ge ) val->u.ival = (cmp>=0);
	    else if ( tok==tt_le ) val->u.ival = (cmp<=0);
	}
	tok = NextToken(c);
    }
    backuptok(c);
}

static void _and(Context *c,Val *val) {
    Val other;
    int old = c->donteval;
    enum token_type tok;

    comp(c,val);
    tok = NextToken(c);
    while ( tok==tt_and || tok==tt_bitand ) {
	other.type = v_void;
	if ( !c->donteval )
	    dereflvalif(c,val);
	if ( tok==tt_and && val->u.ival==0 )
	    c->donteval = true;
	comp(c,&other);
	c->donteval = old;
	if ( !old ) {
	    dereflvalif(c,&other);
	    if ( tok==tt_and && val->type==v_int && val->u.ival==0 )
		val->u.ival = 0;
	    else if ( val->type!=v_int || other.type!=v_int )
		error( c, "Invalid type in integer expression" );
	    else if ( tok==tt_and )
		val->u.ival = val->u.ival && other.u.ival;
	    else
		val->u.ival &= other.u.ival;
	}
	tok = NextToken(c);
    }
    backuptok(c);
}

static void _or(Context *c,Val *val) {
    Val other;
    int old = c->donteval;
    enum token_type tok;

    _and(c,val);
    tok = NextToken(c);
    while ( tok==tt_or || tok==tt_bitor || tok==tt_xor ) {
	other.type = v_void;
	if ( !c->donteval )
	    dereflvalif(c,val);
	if ( tok==tt_or && val->u.ival!=0 )
	    c->donteval = true;
	_and(c,&other);
	c->donteval = old;
	if ( !c->donteval ) {
	    dereflvalif(c,&other);
	    if ( tok==tt_or && val->type==v_int && val->u.ival!=0 )
		val->u.ival = 1;
	    else if ( val->type!=v_int || other.type!=v_int )
		error( c, "Invalid type in integer expression" );
	    else if ( tok==tt_or )
		val->u.ival = val->u.ival || other.u.ival;
	    else if ( tok==tt_bitor )
		val->u.ival |= other.u.ival;
	    else
		val->u.ival ^= other.u.ival;
	}
	tok = NextToken(c);
    }
    backuptok(c);
}

static void assign(Context *c,Val *val) {
    Val other;
    enum token_type tok;

    _or(c,val);
    tok = NextToken(c);
    if ( tok==tt_assign || tok==tt_pluseq || tok==tt_minuseq || tok==tt_muleq || tok==tt_diveq || tok==tt_modeq ) {
	other.type = v_void;
	assign(c,&other);		/* that's the evaluation order here */
	if ( !c->donteval ) {
	    dereflvalif(c,&other);
	    if (( val->type == v_ttf32pt || val->type == v_ttf16pt || val->type == v_ttf8pt) &&
		    other.type == v_int ) {
		int32 temp = other.u.ival;
		if ( tok == tt_assign )
		    setttfpt(c,val,other.u.ival);
		else {
		    temp = derefttfpt(c,val);
		    if ( tok==tt_pluseq ) temp += other.u.ival;
		    else if ( tok==tt_minuseq ) temp -= other.u.ival;
		    else if ( tok==tt_muleq ) temp *= other.u.ival;
		    else if ( other.u.ival==0 )
			error(c,"Divide by zero");
		    else if ( tok==tt_modeq ) temp %= other.u.ival;
		    else temp /= other.u.ival;
		    setttfpt(c,val,temp);
		}
		val->u.ival = temp;
		val->type = v_int;
	    } else if ( val->type!=v_lval )
		error( c, "Expected lvalue" );
	    else if ( other.type == v_void )
		error( c, "Void found on right side of assignment" );
	    else if ( tok==tt_assign ) {
		Val temp;
		int argi;
		temp = *val->u.lval;
		*val->u.lval = other;
		if ( other.type==v_arr )
		    val->u.lval->u.aval = arraycopy(other.u.aval);
		else if ( other.type==v_arrfree )
		    val->u.lval->type = v_arr;
		argi = val->u.lval-c->a.vals;
		/* Have to free things after we copy them */
		if ( argi>=0 && argi<c->a.argc && temp.type==v_arr &&
			temp.u.aval==c->dontfree[argi] )
		    c->dontfree[argi] = NULL;		/* Don't free it */
		else if ( temp.type == v_arr )
		    arrayfree(temp.u.aval);
		else if ( temp.type == v_str )
		    free( temp.u.sval);
	    } else if (( val->u.lval->type==v_int || val->u.lval->type==v_unicode ) && (other.type==v_int || other.type==v_unicode)) {
		if ( tok==tt_pluseq ) val->u.lval->u.ival += other.u.ival;
		else if ( tok==tt_minuseq ) val->u.lval->u.ival -= other.u.ival;
		else if ( tok==tt_muleq ) val->u.lval->u.ival *= other.u.ival;
		else if ( other.u.ival==0 )
		    error(c,"Divide by zero");
		else if ( tok==tt_modeq ) val->u.lval->u.ival %= other.u.ival;
		else val->u.lval->u.ival /= other.u.ival;
	    } else if ( tok==tt_pluseq && val->u.lval->type==v_str &&
		    (other.type==v_str || other.type==v_int)) {
		char *ret, *temp;
		char buffer[10];
		if ( other.type == v_int ) {
		    sprintf(buffer,"%d", other.u.ival);
		    temp = buffer;
		} else
		    temp = other.u.sval;
		ret = galloc(strlen(val->u.lval->u.sval)+strlen(temp)+1);
		strcpy(ret,val->u.lval->u.sval);
		strcat(ret,temp);
		if ( other.type==v_str ) free(other.u.sval);
		free(val->u.lval->u.sval);
		val->u.sval = ret;
	    } else
		error( c, "Invalid types in assignment");
	}
    } else
	backuptok(c);
}

static void expr(Context *c,Val *val) {
    val->type = v_void;
    assign(c,val);
}

static void doforeach(Context *c) {
#if 0
    long here = ctell(c);
    int lineno = c->lineno;
    enum token_type tok;
    int i, selsize;
    char *sel;
    int nest;

    if ( c->curtfv==NULL )
	error(c,"foreach requires an active font");
    selsize = c->curtfv->ttf->charcnt;
    sel = galloc(selsize);
    memcpy(sel,c->curtfv->selected,selsize);
    memset(c->curtfv->selected,0,selsize);
    i = 0;

    while ( 1 ) {
	while ( i<selsize && i<c->curtfv->ttf->charcnt && !sel[i]) ++i;
	if ( i>=selsize || i>=c->curtfv->ttf->charcnt )
    break;
	c->curtfv->selected[i] = true;
	while ( (tok=NextToken(c))!=tt_endloop && tok!=tt_eof && !c->returned ) {
	    backuptok(c);
	    statement(c);
	}
	c->curtfv->selected[i] = false;
	if ( tok==tt_eof )
	    error(c,"End of file found in foreach loop" );
	cseek(c,here);
	c->lineno = lineno;
	++i;
    }

    nest = 0;
    while ( (tok=NextToken(c))!=tt_endloop || nest>0 ) {
	if ( tok==tt_eof )
	    error(c,"End of file found in foreach loop" );
	else if ( tok==tt_while ) ++nest;
	else if ( tok==tt_foreach ) ++nest;
	else if ( tok==tt_endloop ) --nest;
    }
    if ( selsize==c->curtfv->ttf->charcnt )
	memcpy(c->curtfv->selected,sel,selsize);
    free(sel);
#endif
}

static void dowhile(Context *c) {
    long here = ctell(c);
    int lineno = c->lineno;
    enum token_type tok;
    Val val;
    int nest;

    while ( 1 ) {
	tok=NextToken(c);
	expect(c,tt_lparen,tok);
	val.type = v_void;
	expr(c,&val);
	tok=NextToken(c);
	expect(c,tt_rparen,tok);
	dereflvalif(c,&val);
	if ( val.type!=v_int )
	    error( c, "Expected integer expression in while condition");
	if ( val.u.ival==0 )
    break;
	while ( (tok=NextToken(c))!=tt_endloop && tok!=tt_eof && !c->returned ) {
	    backuptok(c);
	    statement(c);
	}
	if ( tok==tt_eof )
	    error(c,"End of file found in while loop" );
	cseek(c,here);
	c->lineno = lineno;
    }

    nest = 0;
    while ( (tok=NextToken(c))!=tt_endloop || nest>0 ) {
	if ( tok==tt_eof )
	    error(c,"End of file found in while loop" );
	else if ( tok==tt_while ) ++nest;
	else if ( tok==tt_foreach ) ++nest;
	else if ( tok==tt_endloop ) --nest;
    }
}

static void doif(Context *c) {
    enum token_type tok;
    Val val;
    int nest;

    while ( 1 ) {
	tok=NextToken(c);
	expect(c,tt_lparen,tok);
	val.type = v_void;
	expr(c,&val);
	tok=NextToken(c);
	expect(c,tt_rparen,tok);
	dereflvalif(c,&val);
	if ( val.type!=v_int )
	    error( c, "Expected integer expression in if condition");
	if ( val.u.ival!=0 ) {
	    while ( (tok=NextToken(c))!=tt_endif && tok!=tt_eof && tok!=tt_else && tok!=tt_elseif && !c->returned ) {
		backuptok(c);
		statement(c);
	    }
	    if ( tok==tt_eof )
		error(c,"End of file found in if statement" );
    break;
	} else {
	    nest = 0;
	    while ( ((tok=NextToken(c))!=tt_endif && tok!=tt_else && tok!=tt_elseif ) || nest>0 ) {
		if ( tok==tt_eof )
		    error(c,"End of file found in if statement" );
		else if ( tok==tt_if ) ++nest;
		else if ( tok==tt_endif ) --nest;
	    }
	    if ( tok==tt_else ) {
		while ( (tok=NextToken(c))!=tt_endif && tok!=tt_eof && !c->returned ) {
		    backuptok(c);
		    statement(c);
		}
    break;
	    } else if ( tok==tt_endif )
    break;
	}
    }
    if ( c->returned )
return;
    if ( tok!=tt_endif && tok!=tt_eof ) {
	nest = 0;
	while ( (tok=NextToken(c))!=tt_endif || nest>0 ) {
	    if ( tok==tt_eof )
return;
	    else if ( tok==tt_if ) ++nest;
	    else if ( tok==tt_endif ) --nest;
	}
    }
}

static void doshift(Context *c) {
    int i;

    if ( c->a.argc==1 )
	error(c,"Attempt to shift when there are no arguments left");
    if ( c->a.vals[1].type==v_str )
	free(c->a.vals[1].u.sval );
    if ( c->a.vals[1].type==v_arr && c->a.vals[1].u.aval != c->dontfree[1] )
	arrayfree(c->a.vals[1].u.aval );
    --c->a.argc;
    for ( i=1; i<c->a.argc ; ++i ) {
	c->a.vals[i] = c->a.vals[i+1];
	c->dontfree[i] = c->dontfree[i+1];
    }
}

static void statement(Context *c) {
    enum token_type tok = NextToken(c);
    Val val;

    if ( tok==tt_while )
	dowhile(c);
    else if ( tok==tt_foreach )
	doforeach(c);
    else if ( tok==tt_if )
	doif(c);
    else if ( tok==tt_shift )
	doshift(c);
    else if ( tok==tt_else || tok==tt_elseif || tok==tt_endif || tok==tt_endloop ) {
	unexpected(c,tok);
    } else if ( tok==tt_return ) {
	tok = NextToken(c);
	backuptok(c);
	c->returned = true;
	c->return_val.type = v_void;
	if ( tok!=tt_eos ) {
	    expr(c,&c->return_val);
	    dereflvalif(c,&c->return_val);
	    if ( c->return_val.type==v_arr ) {
		c->return_val.type = v_arrfree;
		c->return_val.u.aval = arraycopy(c->return_val.u.aval);
	    }
	}
    } else if ( tok==tt_eos ) {
	backuptok(c);
    } else {
	backuptok(c);
	expr(c,&val);
	if ( val.type == v_str )
	    free( val.u.sval );
    }
    tok = NextToken(c);
    if ( tok!=tt_eos && tok!=tt_eof && !c->returned )
	error( c, "Unterminated statement" );
}

static FILE *CopyNonSeekableFile(FILE *former) {
    int ch = '\n';
    FILE *temp = tmpfile();
    int istty = isatty(fileno(former)) && former==stdin;

    if ( temp==NULL )
return( former );
    if ( istty )
	printf( "Type in your script file. Processing will not begin until all the script\n" );
	printf( " has been input (ie. until you have pressed ^D)\n" );
    while ( 1 ) {
	if ( ch=='\n' && istty )
	    printf( "> " );
	ch = getc(former);
	if ( ch==EOF )
    break;
	putc(ch,temp);
    }
    if ( istty )
	printf( "\n" );
    rewind(temp);
return( temp );
}

static void VerboseCheck(void) {
    if ( verbose==-1 )
	verbose = getenv("MENSIS_VERBOSE")!=NULL;
}

static void ProcessScript(int argc, char *argv[], FILE *script) {
    int i,j;
    Context c;
    enum token_type tok;

    VerboseCheck();

    i=1;
    if ( script!=NULL ) {
	if ( argc<2 || strcmp(argv[1],"-")!=0 )
	    i = 0;
    } else if ( strcmp(argv[1],"-nosplash")==0 || strcmp(argv[1],"--nosplash")==0 ) {
	++i;
	if ( strcmp(argv[2],"-script")==0 || strcmp(argv[2],"--script")==0 )
	    ++i;
    } else if ( strcmp(argv[1],"-script")==0 || strcmp(argv[1],"--script")==0 )
	++i;
    memset( &c,0,sizeof(c));
    c.a.argc = argc-i;
    c.a.vals = galloc(c.a.argc*sizeof(Val));
    c.dontfree = gcalloc(c.a.argc,sizeof(Array*));
    for ( j=i; j<argc; ++j ) {
	c.a.vals[j-i].type = v_str;
	c.a.vals[j-i].u.sval = copy(argv[j]);
    }
    c.return_val.type = v_void;
    if ( script!=NULL ) {
	c.filename = "<stdin>";
	c.script = script;
    } else if ( i<argc && strcmp(argv[i],"-")!=0 ) {
	c.filename = argv[i];
	c.script = fopen(c.filename,"r");
    } else {
	c.filename = "<stdin>";
	c.script = stdin;
    }
    /* On Mac OS/X fseek/ftell appear to be broken and return success even */
    /*  for terminals. They should return -1, EBADF */
    if ( c.script!=NULL && (ftell(c.script)==-1 || isatty(fileno(c.script))) )
	c.script = CopyNonSeekableFile(c.script);
    if ( c.script==NULL )
	error(&c, "No such file");
    else {
	c.lineno = 1;
	while ( !c.returned && (tok = NextToken(&c))!=tt_eof ) {
	    backuptok(&c);
	    statement(&c);
	}
	fclose(c.script);
    }
    for ( i=0; i<c.a.argc; ++i )
	free(c.a.vals[i].u.sval);
    free(c.a.vals);
    free(c.dontfree);
    exit(0);
}

static void _CheckIsScript(int argc, char *argv[]) {
    if ( argc==1 )
return;
    if ( strcmp(argv[1],"-script")==0 || strcmp(argv[1],"--script")==0 )
	ProcessScript(argc, argv,NULL);
    else if ( (strcmp(argv[1],"-nosplash")==0 || strcmp(argv[1],"--nosplash")==0) &&
	    argc>=3 && ( strcmp(argv[2],"-script")==0 || strcmp(argv[2],"--script")==0 ))
	ProcessScript(argc, argv,NULL);
    if ( access(argv[1],X_OK|R_OK)==0 ) {
	FILE *temp = fopen(argv[1],"r");
	char buffer[200];
	if ( temp==NULL )
return;
	buffer[0] = '\0';
	fgets(buffer,sizeof(buffer),temp);
	fclose(temp);
	if ( buffer[0]=='#' && buffer[1]=='!' && strstr(buffer,"mensis")!=NULL )
	    ProcessScript(argc, argv,NULL);
    }
    if ( strcmp(argv[1],"-")==0 )	/* Someone thought that, of course, "-" meant read from a script. I guess it makes no sense with anything else... */
	ProcessScript(argc, argv,stdin);
}

#ifdef X_DISPLAY_MISSING
static void _doscriptusage(void) {
    printf( "mensis [options]\n" );
    printf( "\t-usage\t\t\t (displays this message, and exits)\n" );
    printf( "\t-help\t\t\t (displays this message, invokes a browser)\n\t\t\t\t  (Using the BROWSER environment variable)\n" );
    printf( "\t-version\t\t (prints the version of mensis and exits)\n" );
    printf( "\t-script scriptfile\t (executes scriptfile)\n" );
    printf( "\n" );
    printf( "If no scriptfile is given (or if it's \"-\") Mensis will read stdin\n" );
    printf( "Any arguments after the script file will be passed to it.\n");
    printf( "If the first argument is an executable filename, and that file's first\n" );
    printf( "\tline contains \"mensis\" then it will be treated as a scriptfile.\n\n" );
    printf( "For more information see:\n\thttp://mensis.sourceforge.net/\n" );
    printf( "Send bug reports to:\tmensis-devel@lists.sourceforge.net\n" );
}

static void doscriptusage(void) {
    _doscriptusage();
exit(0);
}

static void doscripthelp(void) {
    _doscriptusage();
    help("overview.html");
exit(0);
}
#endif

void CheckIsScript(int argc, char *argv[]) {
    _CheckIsScript(argc, argv);
#ifdef X_DISPLAY_MISSING
    if ( argc==2 ) {
	char *pt = argv[1];
	if ( *pt=='-' && pt[1]=='-' ) ++pt;
	if ( strcmp(pt,"-usage")==0 )
	    doscriptusage();
	else if ( strcmp(pt,"-help")==0 )
	    doscripthelp();
	else if ( strcmp(pt,"-version")==0 )
	    doversion();
    }
    ProcessScript(argc, argv,stdin);
#endif
}
#if 0
void ExecuteScriptFile(TtfView *tfv, char *filename) {
    Context c;
    Val argv[1];
    Array *dontfree[1];
    enum token_type tok;
    jmp_buf env;

    VerboseCheck();

    memset( &c,0,sizeof(c));
    c.a.argc = 1;
    c.a.vals = argv;
    c.dontfree = dontfree;
    argv[0].type = v_str;
    argv[0].u.sval = filename;
    c.filename = filename;
    c.return_val.type = v_void;
    c.err_env = &env;
    c.curtfv = tfv;
    if ( setjmp(env)!=0 )
return;				/* Error return */

    c.script = fopen(c.filename,"r");
    if ( c.script==NULL )
	error(&c, "No such file");
    else {
	c.lineno = 1;
	while ( !c.returned && (tok = NextToken(&c))!=tt_eof ) {
	    backuptok(&c);
	    statement(&c);
	}
	fclose(c.script);
    }
}

struct sd_data {
    int done;
    FontView *fv;
    GWindow gw;
};

#define SD_Width	250
#define SD_Height	270
#define CID_Script	1001

static int SD_Call(GGadget *g, GEvent *e) {
    if ( e->type==et_controlevent && e->u.control.subtype == et_buttonactivate ) {
	static unichar_t filter[] = { '*',/*'.','p','e',*/  0 };
	unichar_t *fn;
	unichar_t *insert;
    
	fn = GWidgetOpenFile(GStringGetResource(_STR_CallScript,NULL), NULL, filter, NULL, NULL);
	if ( fn==NULL )
return(true);
	insert = galloc((u_strlen(fn)+10)*sizeof(unichar_t));
	*insert = '"';
	u_strcpy(insert+1,fn);
	uc_strcat(insert,"\"()");
	GTextFieldReplace(GWidgetGetControl(GGadgetGetWindow(g),CID_Script),insert);
	free(insert);
	free(fn);
    }
return( true );
}

static int SD_OK(GGadget *g, GEvent *e) {
    if ( e->type==et_controlevent && e->u.control.subtype == et_buttonactivate ) {
	struct sd_data *sd = GDrawGetUserData(GGadgetGetWindow(g));
	Context c;
	Val args[1];
	Array *dontfree[1];
	jmp_buf env;
	enum token_type tok;

	memset( &c,0,sizeof(c));
	memset( args,0,sizeof(args));
	memset( dontfree,0,sizeof(dontfree));
	c.a.argc = 1;
	c.a.vals = args;
	c.dontfree = dontfree;
	c.filename = args[0].u.sval = "ScriptDlg";
	args[0].type = v_str;
	c.return_val.type = v_void;
	c.err_env = &env;
	c.curtfv = sd->fv;
	if ( setjmp(env)!=0 )
return( true );			/* Error return */

	c.script = tmpfile();
	if ( c.script==NULL )
	    error(&c, "Can't create temporary file");
	else {
	    const unichar_t *ret = _GGadgetGetTitle(GWidgetGetControl(sd->gw,CID_Script));
	    while ( *ret ) {
		/* There's a bug here. Filenames need to be converted to the local charset !!!! */
		putc(*ret,c.script);
		++ret;
	    }
	    rewind(c.script);
	    VerboseCheck();
	    c.lineno = 1;
	    while ( !c.returned && (tok = NextToken(&c))!=tt_eof ) {
		backuptok(&c);
		statement(&c);
	    }
	    fclose(c.script);
	    sd->done = true;
	}
    }
return( true );
}

static void SD_DoCancel(struct sd_data *sd) {
    sd->done = true;
}

static int SD_Cancel(GGadget *g, GEvent *e) {
    if ( e->type==et_controlevent && e->u.control.subtype == et_buttonactivate ) {
	SD_DoCancel( GDrawGetUserData(GGadgetGetWindow(g)));
    }
return( true );
}

static int sd_e_h(GWindow gw, GEvent *event) {
    if ( event->type==et_close ) {
	SD_DoCancel( GDrawGetUserData(gw));
    } else if ( event->type==et_char ) {
	if ( event->u.chr.keysym == GK_F1 || event->u.chr.keysym == GK_Help ) {
	    help("scripting.html");
return( true );
	}
return( false );
    } else if ( event->type == et_map ) {
	/* Above palettes */
	GDrawRaise(gw);
    }
return( true );
}

void ScriptDlg(FontView *fv) {
    GRect pos;
    static GWindow gw;
    GWindowAttrs wattrs;
    GGadgetCreateData gcd[10];
    GTextInfo label[10];
    struct sd_data sd;
    TtfView *list;

    memset(&sd,0,sizeof(sd));
    sd.fv = fv;

    if ( gw==NULL ) {
	memset(&wattrs,0,sizeof(wattrs));
	wattrs.mask = wam_events|wam_cursor|wam_wtitle|wam_undercursor|wam_restrict|wam_isdlg;
	wattrs.event_masks = ~(1<<et_charup);
	wattrs.restrict_input_to_me = 1;
	wattrs.undercursor = 1;
	wattrs.cursor = ct_pointer;
	wattrs.window_title = GStringGetResource(_STR_ExecuteScript,NULL);
	wattrs.is_dlg = true;
	pos.x = pos.y = 0;
	pos.width = GDrawPointsToPixels(NULL,GGadgetScale(SD_Width));
	pos.height = GDrawPointsToPixels(NULL,SD_Height);
	gw = GDrawCreateTopWindow(NULL,&pos,sd_e_h,&sd,&wattrs);

	memset(&gcd,0,sizeof(gcd));
	memset(&label,0,sizeof(label));

	gcd[0].gd.pos.x = 10; gcd[0].gd.pos.y = 10;
	gcd[0].gd.pos.width = SD_Width-20; gcd[0].gd.pos.height = SD_Height-54;
	gcd[0].gd.flags = gg_visible | gg_enabled | gg_textarea_wrap;
	gcd[0].gd.cid = CID_Script;
	gcd[0].creator = GTextAreaCreate;

	gcd[1].gd.pos.x = 25-3; gcd[1].gd.pos.y = SD_Height-32-3;
	gcd[1].gd.pos.width = -1; gcd[1].gd.pos.height = 0;
	gcd[1].gd.flags = gg_visible | gg_enabled | gg_but_default;
	label[1].text = (unichar_t *) _STR_OK;
	label[1].text_in_resource = true;
	gcd[1].gd.mnemonic = 'O';
	gcd[1].gd.label = &label[1];
	gcd[1].gd.handle_controlevent = SD_OK;
	gcd[1].creator = GButtonCreate;

	gcd[2].gd.pos.x = -25; gcd[2].gd.pos.y = SD_Height-32;
	gcd[2].gd.pos.width = -1; gcd[2].gd.pos.height = 0;
	gcd[2].gd.flags = gg_visible | gg_enabled | gg_but_cancel;
	label[2].text = (unichar_t *) _STR_Cancel;
	label[2].text_in_resource = true;
	gcd[2].gd.label = &label[2];
	gcd[2].gd.mnemonic = 'C';
	gcd[2].gd.handle_controlevent = SD_Cancel;
	gcd[2].creator = GButtonCreate;

	gcd[3].gd.pos.x = (SD_Width-GIntGetResource(_NUM_Buttonsize)*100/GIntGetResource(_NUM_ScaleFactor))/2; gcd[3].gd.pos.y = SD_Height-40;
	gcd[3].gd.pos.width = -1; gcd[3].gd.pos.height = 0;
	gcd[3].gd.flags = gg_visible | gg_enabled;
	label[3].text = (unichar_t *) _STR_Call;
	label[3].text_in_resource = true;
	gcd[3].gd.label = &label[3];
	gcd[3].gd.mnemonic = 'a';
	gcd[3].gd.handle_controlevent = SD_Call;
	gcd[3].creator = GButtonCreate;

	gcd[4].gd.pos.x = 2; gcd[4].gd.pos.y = 2;
	gcd[4].gd.pos.width = pos.width-4; gcd[4].gd.pos.height = pos.height-4;
	gcd[4].gd.flags = gg_enabled | gg_visible | gg_pos_in_pixels;
	gcd[4].creator = GGroupCreate;

	GGadgetsCreate(gw,gcd);
    }
    sd.gw = gw;
    GDrawSetUserData(gw,&sd);
    GDrawSetVisible(gw,true);
    while ( !sd.done )
	GDrawProcessOneEvent(NULL);
    GDrawSetVisible(gw,false);

    /* Selection may be out of date, force a refresh */
    for ( list = tfv_list; list!=NULL; list=list->next )
	GDrawRequestExpose(list->v,NULL,false);
}
#endif
