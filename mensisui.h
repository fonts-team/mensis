/* Copyright (C) 2001-2003 by George Williams */
/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.

 * The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _MENSISUI_H_
#define _MENSISUI_H_

#include "mensis.h"
#include <fontforge/gdraw.h>
#include <fontforge/gwidget.h>
#include <fontforge/ggadget.h>
#include "ttfview.h"
#include <time.h>

extern TtfView *tfv_list;
extern GWindow ttf_icon;
extern GCursor ct_leftright;

extern void InitCursors(void);

extern void TimeTToQuad(time_t t, uint32 date[2]);
extern real GetRealR(GWindow gw,int cid,int namer,int *err);
extern int GetIntR(GWindow gw,int cid,int namer,int *err);
extern int GetHexR(GWindow gw,int cid,int namer,int *err);
extern int GetListR(GWindow gw,int cid,int namer,int *err);
extern void GetDateR(GWindow gw,int cid,int namer,uint32 date[2], int *err);
extern void ProtestR(int labelr);
extern void help(char *);

#endif
