/* Copyright (C) 2001-2003 by George Williams */
/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.

 * The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "mensisui.h"
#include <fontforge/ustring.h>
#include <fontforge/utype.h>

    TableFillup(tab);
table version = tgetfixed(tab,0);
fontnumber = tgetlong(tab,4);	/* hex */
pitch = tgetushort(tab,8);	/* width of space */
xHeight = tgetushort(tab,10);
style = tgetushort(tab,12);	/* hex */
typefamily = tgetushort(tab,14);	/* hex */
capheight = tgetushort(tab,16);
symbolset = tgetushort(tab,18);
typeface = tgetstring(tab,20,16);
charactercomplement = tgetstring(tab,36,8); /* hex */
filename = tgetstring(tab,44,6);
strokeweight = tab->data[50];	/* signed */
widthtype = tab->data[51];	/* signed */
serifstyle = tab->data[52];	/* hex */
mbz = tab->data[53];
